# Projet blazor

## Présentation
Le projet de notre dépôt contient tous les éléments de la consignes (hors bonus), indiqué à [cette adresse](https://codefirst.iut.uca.fr/documentation/julien.riboulet/docusaurus/Blazor/your-next-steps).  

## Lancer le projet

Plusieurs méthodes sont disponibles pour lancer le projet.

### Via Docker
Une version déployée du projet est déjà disponible à l'URL https://codefirst.iut.uca.fr/containers/projet-blazor-aurianjault


### Via Visual Studio

#### Lancez VS

Lancez visual Studio, cliquez, sur la droite, "Clôner un dépôt".

#### Cloner

Renseigner dans le champ "Emplacement du dêpôt" ce lien: `https://codefirst.iut.uca.fr/git/remi.arnal/blazor.git`  
Une fois que vous avez choisi le chemin de votre choix pour le clonage, appuyez sur Cloner.

#### Lancer la solution

Une fois le dépôt cloné, lancez la solution en cliquant sur le triangle vert plein situé dans la barre d'outil en haut de votre écran.

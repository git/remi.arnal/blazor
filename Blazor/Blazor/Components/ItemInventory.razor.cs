﻿using Blazor.Models;
using Blazored.LocalStorage;
using Blazorise.Extensions;
using Microsoft.AspNetCore.Components;

namespace Blazor.Components
{
    public partial class ItemInventory
    {
        [Parameter]
        public bool NoDrop { get; set; }

        [Parameter]
        public InventoryItem Items { get; set; }

        [Parameter]
        public int Index { get; set; }

        [CascadingParameter]
        public Inventory Parent { get; set; }

        [Inject]
        public ILocalStorageService LocalStorage { get; set; }

        public async Task OnDragStartAsync()
        {

            if (this.Items == null)
            {
                Parent.CurrentDragItem = null;
                return;
            }

            Parent.CurrentDragItem = this.Items;
            Parent.Actions.Add(new InventoryAction("On drag start",this.Index,Items.item));
            this.Items = null;
            Parent.inventory.inventoryItems[this.Index] = null ;
            await LocalStorage.RemoveItemAsync("data" + this.Index);
            await LocalStorage.RemoveItemAsync("stack" + this.Index);
        }
        

        public async Task OnDropAsync()
        {

            if (Parent.CurrentDragItem == null)
            {
                return;
            }

            if(this.Items == null)
            {
                this.Items = Parent.CurrentDragItem;
                Parent.CurrentDragItem = null;
                await LocalStorage.SetItemAsync<Item>("data" + this.Index, this.Items.item);
                await LocalStorage.SetItemAsync<int>("stack" + this.Index, this.Items.Stack);
                Parent.Actions.Add(new InventoryAction("On drag drop",this.Index,Items.item));
                return;
            }

            if (Parent.CurrentDragItem.item.Id != this.Items.item.Id)
            {
                this.Items = Parent.CurrentDragItem;
                Parent.CurrentDragItem = null;
                await LocalStorage.SetItemAsync<Item>("data" + this.Index, this.Items.item);
                await LocalStorage.SetItemAsync<int>("stack" + this.Index, this.Items.Stack);
                Parent.Actions.Add(new InventoryAction("On drag drop",this.Index,Items.item));
                return;
            }
            else
            {       
                int total = Parent.CurrentDragItem.Stack + this.Items.Stack;
                if (total > this.Items.item.StackSize)
                {
                    this.Items.Stack = this.Items.item.StackSize;
                    Parent.CurrentDragItem = null;

                    await LocalStorage.SetItemAsync<Item>("data" + this.Index, this.Items.item);
                    await LocalStorage.SetItemAsync<int>("stack" + this.Index, this.Items.Stack);
                    Parent.Actions.Add(new InventoryAction("On drag drop",this.Index,Items.item));
                    return;
                }
                else
                {
                    this.Items.Stack = total;
                    Parent.CurrentDragItem = null;
                    await LocalStorage.SetItemAsync<Item>("data" + this.Index, this.Items.item);
                    await LocalStorage.SetItemAsync<int>("stack" + this.Index, this.Items.Stack);
                    Parent.Actions.Add(new InventoryAction("On drag drop",this.Index,Items.item));
                    return;
                }
            }
        }
        internal void OnDragEnter()
        {
            if (this.Items == null) return;
            Parent.Actions.Add(new InventoryAction("Drag Enter",this.Index,Items.item));
        }

        internal void OnDragLeave()
        {
            if (this.Items == null) return;
            Parent.Actions.Add(new InventoryAction("Drag Leave",this.Index,Items.item));
        }


        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            base.OnAfterRenderAsync(firstRender);

            if (!firstRender)
            {
                return;
            }
            var item = await LocalStorage.GetItemAsync<Item>("data" + this.Index);
            int stack = await LocalStorage.GetItemAsync<int>("stack" + this.Index);

            if (item != null)
            {
                this.Items = new Models.InventoryItem(item, stack);
            }
            else
            {
                this.Items = null;
            }
                StateHasChanged();
                return;
        }
    }
}

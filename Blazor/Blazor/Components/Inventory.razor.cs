﻿using Blazor.Models;
using Blazor.Pages;
using Blazored.LocalStorage;
using Blazorise.DataGrid;
using Blazorise;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Text.Json;
using Blazor.Data;

namespace Blazor.Components
{
    partial class Inventory
    {
        public Models.InventoryList inventory = new Models.InventoryList();
        public Models.InventoryItem CurrentDragItem { get; set; }
        public ObservableCollection<InventoryAction> Actions { get; set; }

        [Inject]
        internal IJSRuntime JavaScriptRuntime { get; set; }

        [Inject]
        public ILocalStorageService LocalStorage { get; set; }
        
        [Inject]
        public HttpClient Http { get; set; }

        public Inventory() 
        {
            Actions = new ObservableCollection<InventoryAction>();
            Actions.CollectionChanged += OnActionsCollectionChanged;
        }
        public async void update()
        {
            this.StateHasChanged();
            var serial = JsonSerializer.Serialize(inventory);
            await LocalStorage.SetItemAsync<String>("list", serial);
            return;
        }


        private void OnActionsCollectionChanged(object? sender, NotifyCollectionChangedEventArgs e)
        {
            JavaScriptRuntime.InvokeVoidAsync("Crafting.AddActions", e.NewItems);
        }



        private async Task OnReadData(DataGridReadDataEventArgs<Item> e)
        {
            if (e.CancellationToken.IsCancellationRequested)
            {
                return;
            }

            var response = await LocalStorage.GetItemAsync<String>("list");

            if (!e.CancellationToken.IsCancellationRequested)
            {
                inventory = JsonSerializer.Deserialize<Models.InventoryList>(response);
            }
        }

    }
}

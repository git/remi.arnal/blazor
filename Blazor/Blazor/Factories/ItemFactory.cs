﻿using Blazor.Models;

namespace Blazor.Factories
{   
    /// <summary>
    /// A static factory for creating and updating ItemModel and Item.
    /// </summary>
    public static class ItemFactory
    {   
        /// <summary>
        /// Converts an Item to an ItemModel
        /// </summary>
        /// <param name="item">The item to convert.</param>
        /// <param name="imageContent">The image content of the ItemModel</param>
        /// <returns>An ItemModel with the same properties as the item.</returns>
        public static ItemModel ToModel(Item item, byte[] imageContent)
        {   
            return new ItemModel
            {
                Id = item.Id,
                DisplayName = item.DisplayName,
                Name = item.Name,
                RepairWith = item.RepairWith,
                EnchantCategories = item.EnchantCategories,
                MaxDurability = item.MaxDurability,
                StackSize = item.StackSize,
                ImageContent = imageContent,
                ImageBase64 = string.IsNullOrWhiteSpace(item.ImageBase64) ? Convert.ToBase64String(imageContent) : item.ImageBase64
            };
        }

        /// <summary>
        /// Creates an Item from an ItemModel.
        /// </summary>
        /// <param name="model">The ItemModel to create the Itemfrom.</param>
        /// <returns>An Item with the same properties as the ItemModel.</returns>
        public static Item Create(ItemModel model)
        {
            return new Item
            {
                Id = model.Id,
                DisplayName = model.DisplayName,
                Name = model.Name,
                RepairWith = model.RepairWith,
                EnchantCategories = model.EnchantCategories,
                MaxDurability = model.MaxDurability,
                StackSize = model.StackSize,
                CreatedDate = DateTime.Now,
                ImageBase64 = Convert.ToBase64String(model.ImageContent)
            };
        }
        
        /// <summary>
        /// Updates an Item with the properties of an ItemModel.
        /// </summary>
        /// <param name="item"> The Item to update </param>
        /// <param name="model"> The ItemModel used to update the item </param>
        public static void Update(Item item, ItemModel model)
        {
            item.DisplayName = model.DisplayName;
            item.Name = model.Name;
            item.RepairWith = model.RepairWith;
            item.EnchantCategories = model.EnchantCategories;
            item.MaxDurability = model.MaxDurability;
            item.StackSize = model.StackSize;
            item.UpdatedDate = DateTime.Now;
            item.ImageBase64 = Convert.ToBase64String(model.ImageContent);
        }
    }
}

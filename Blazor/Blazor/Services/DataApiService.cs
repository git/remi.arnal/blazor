﻿using Blazor.Components;
using Blazor.Factories;
using Blazor.Models;

namespace Blazor.Services
{   
    /// <summary>
    /// Service for interacting with a data API.
    /// </summary>
    public class DataApiService : IDataService
    {
        private readonly HttpClient _http;

        /// <summary>
        /// Constructor for DataApiService.
        /// </summary>
        /// <param name="http">HttpClient for making API requests.</param>
        public DataApiService(HttpClient http)
        {
            _http = http;
        }

        /// <summary>
        /// Add a new item to the API.
        /// </summary>
        /// <param name="model">Model containing data for the new item.</param>
        public async Task Add(ItemModel model)
        {
            // Get the item
            var item = ItemFactory.Create(model);

            // Save the data
            await _http.PostAsJsonAsync("https://codefirst.iut.uca.fr/containers/container-blazor-web-api-marcchevaldonne/api/Crafting/", item);
        }

        /// <summary>
        /// Get the number of items in the API.
        /// </summary>
        /// <returns>Task representing the asynchronous operation, returning the number of items.</returns>
        public async Task<int> Count()
        {
            return await _http.GetFromJsonAsync<int>("https://codefirst.iut.uca.fr/containers/container-blazor-web-api-marcchevaldonne/api/Crafting/count");
        }

        /// <summary>
        /// Get a list of items from the API, paginated.
        /// </summary>
        /// <param name="currentPage">The current page number.</param>
        /// <param name="pageSize">The number of items per page.</param>
        /// <returns>Task representing the asynchronous operation, returning a list of items.</returns>
        public async Task<List<Item>> List(int currentPage, int pageSize)
        {
            return await _http.GetFromJsonAsync<List<Item>>($"https://codefirst.iut.uca.fr/containers/container-blazor-web-api-marcchevaldonne/api/Crafting/?currentPage={currentPage}&pageSize={pageSize}");
        }

        /// <summary>
        /// Get a list of all items from the API.
        /// </summary>
        /// <returns>Task representing the asynchronous operation, returning a list of items.</returns>
        public async Task<List<Item>> getAll()
        {
            return await _http.GetFromJsonAsync<List<Item>>($"https://codefirst.iut.uca.fr/containers/container-blazor-web-api-marcchevaldonne/api/Crafting/all");
        }

        /// <summary>
        /// Get a single item from the API by its ID.
        /// </summary>
        /// <param name="id">The ID of the item to retrieve.</param>
        /// <returns>Task representing the asynchronous operation, returning the item with the specified ID.</returns>
        public async Task<Item> GetById(int id)
        {
            return await _http.GetFromJsonAsync<Item>($"https://codefirst.iut.uca.fr/containers/container-blazor-web-api-marcchevaldonne/api/Crafting/{id}");
        }

        /// <summary>
        /// Update an existing item in the API.
        /// </summary>
        /// <param name="id">The ID of the item to update.</param>
        /// <param name="model">Model containing the updated data for the item.</param>
        public async Task Update(int id, ItemModel model)
        {
            // Get the item
            var item = ItemFactory.Create(model);

            await _http.PutAsJsonAsync($"https://codefirst.iut.uca.fr/containers/container-blazor-web-api-marcchevaldonne/api/Crafting/{id}", item);
        }

        /// <summary>
        /// Delete an existing item from the API.
        /// </summary>
        /// <param name="id">The ID of the item to delete.</param>
        public async Task Delete(int id)
        {
            await _http.DeleteAsync($"https://codefirst.iut.uca.fr/containers/container-blazor-web-api-marcchevaldonne/api/Crafting/{id}");
        }

        /// <summary>
        /// Get a list of crafting recipes from the API.
        /// </summary>
        /// <returns>Task representing the asynchronous operation, returning a list of crafting recipes.</returns>
        public async Task<List<CraftingRecipe>> GetRecipes()
        {
            return await _http.GetFromJsonAsync<List<CraftingRecipe>>("https://codefirst.iut.uca.fr/containers/container-blazor-web-api-marcchevaldonne/api/Crafting/recipe");
        }
    }
}

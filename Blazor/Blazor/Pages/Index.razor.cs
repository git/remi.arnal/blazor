﻿using Blazor.Components;
using Blazor.Models;
using Blazor.Services;
using Microsoft.AspNetCore.Components;

namespace Blazor.Pages
{   
    /// <summary>
    /// Partial class for the Index page.
    /// </summary>
    public partial class Index
    {   
        /// <summary>
        /// The data service for accessing item and recipe data.
        /// </summary>
        [Inject]
        public IDataService DataService { get; set; }

        /// <summary>
        /// List of items to display on the page.
        /// </summary>
        public List<Item> Items { get; set; } = new List<Item>();

        /// <summary>
        /// List of crafting recipes possible.
        /// </summary>
        private List<CraftingRecipe> Recipes { get; set; } = new List<CraftingRecipe>();

        /// <summary>
        /// Method that runs after the component has finished rendering.
        /// </summary>
        /// <param name="firstRender">Indicates whether this is the first render of the component.</param>
        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            base.OnAfterRenderAsync(firstRender);

            if (!firstRender)
            {
                return;
            }

            Items = await DataService.List(0, await DataService.Count());
            Recipes = await DataService.GetRecipes();

            StateHasChanged();
        }
    }
}

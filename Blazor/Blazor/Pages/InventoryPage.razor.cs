﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Localization;

namespace Blazor.Pages
{   
    /// <summary>
    /// Partial class for the InventoryPage.
    /// </summary>
    public partial class InventoryPage
    {
        /// <summary>
        /// The localizer for the InventoryPage.
        /// </summary>
        [Inject]
        public IStringLocalizer<InventoryPage> Localizer { get; set; }

    }
}

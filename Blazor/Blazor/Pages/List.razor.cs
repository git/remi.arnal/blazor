﻿using Blazor.Modals;
using Blazor.Models;
using Blazor.Services;
using Blazored.LocalStorage;
using Blazored.Modal;
using Blazored.Modal.Services;
using Blazorise.DataGrid;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Localization;

namespace Blazor.Pages
{   
    /// <summary>
    /// Partial class for the List page.
    /// </summary>
    public partial class List
    {   
        private List<Item> items;

        private int totalItem;

        [Inject]
        public IDataService DataService { get; set; }

        [Inject]
        public IWebHostEnvironment WebHostEnvironment { get; set; }

        /// <summary>
        /// The navigation manager for navigating between pages.
        /// </summary>
        [Inject]
        public NavigationManager NavigationManager { get; set; }
        
        /// <summary>
        /// The modal service for displaying modal dialogs
        /// </summary>
        [CascadingParameter]
        public IModalService Modal { get; set; }

        /// <summary>
        /// The localizer for the List page.
        /// </summary>
        [Inject]
        public IStringLocalizer<List> Localizer { get; set; }

        /// <summary>
        /// Event handler for reading data in the data grid.
        /// </summary>
        /// <param name="e">The event arguments.</param>
        /// <returns>A task representing the asynchronous operation.</returns>
        private async Task OnReadData(DataGridReadDataEventArgs<Item> e)
        {
            if (e.CancellationToken.IsCancellationRequested)
            {
                return;
            }

            if (!e.CancellationToken.IsCancellationRequested)
            {
                items = await DataService.List(e.Page, e.PageSize);
                totalItem = await DataService.Count();
            }
        }

        /// <summary>
        /// Event handler for deleting an item.
        /// </summary>
        /// <param name="id">The id of the item to delete.</param>
        private async void OnDelete(int id)
        {
            var parameters = new ModalParameters();
            parameters.Add(nameof(Item.Id), id);

            var modal = Modal.Show<DeleteConfirmation>("Delete Confirmation", parameters);
            var result = await modal.Result;

            if (result.Cancelled)
            {
                return;
            }

            await DataService.Delete(id);

            // Reload the page
            NavigationManager.NavigateTo("list", true);
        }
    }
}
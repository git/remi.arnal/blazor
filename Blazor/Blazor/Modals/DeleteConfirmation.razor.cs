﻿using Blazor.Models;
using Blazor.Services;
using Blazored.Modal;
using Blazored.Modal.Services;
using Microsoft.AspNetCore.Components;

namespace Blazor.Modals
{   
    /// <summary>
    /// Partial class for the delete confirmation modal.
    /// </summary>
    public partial class DeleteConfirmation
    {   
        /// <summary>
        /// The modal instance
        /// </summary>
        [CascadingParameter]
        public BlazoredModalInstance ModalInstance { get; set; }


        [Inject]
        public IDataService DataService { get; set; }

        /// <summary>
        /// The id of the item to delete
        /// </summary>
        [Parameter]
        public int Id { get; set; }


        private Item item = new Item();

        protected override async Task OnInitializedAsync()
        {
            // Get the item
            item = await DataService.GetById(Id);
        }

        /// <summary>
        /// Confirms the deletion of the item.
        /// </summary>
        void ConfirmDelete()
        {
            ModalInstance.CloseAsync(ModalResult.Ok(true));
        }

        /// <summary>
        /// Cancels the deletion of the item.
        /// </summary>
        void Cancel()
        {
            ModalInstance.CancelAsync();
        }
    }
}

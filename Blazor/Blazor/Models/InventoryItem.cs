﻿using System.Collections;

namespace Blazor.Models
{   
    /// <summary>
    /// Represents an item in a player's inventory.
    /// </summary>
    public class InventoryItem
    {
        public Item item;
        int stack;

        /// <summary>
        /// The number of items in the stack.
        /// </summary>
        public int Stack { get; set; }

        /// <summary>
        /// Constructor for InventoryItem with no parameters.
        /// </summary>
        public InventoryItem()
        {
            item = new Item();
            Stack = 64;
        }
        /// <summary>
        /// Constructor for InventoryItem with a single item.
        /// </summary>
        /// <param name="item">The item.</param>
        public InventoryItem(Item item)
        {
            this.item = item;
            Stack = 1;
        }

        /// <summary>
        /// Constructor for InventoryItem with a stack of items.
        /// </summary>
        /// <param name="item">The item</param>
        /// <param name="stock">The number of items in the stack.</param>
        public InventoryItem(Item item, int stock)
        {
            this.item = item;
            Stack = stock;
        }
    }
}

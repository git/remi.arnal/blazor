﻿using Microsoft.AspNetCore.Http.Features;

namespace Blazor.Models
{   
    /// <summary>
    /// Represents a list of items in a player's inventory.
    /// </summary>
    public partial class InventoryList
    {   

        static public int size = 18;

        /// <summary>
        /// List of inventory items.
        /// </summary>
        public List<InventoryItem> inventoryItems = new List<InventoryItem>(new InventoryItem[size]);

        /// <summary>
        /// Constructor for InventoryList.
        /// </summary>
        public InventoryList() 
        {
            inventoryItems[0] = new InventoryItem();
        }
    }
}

﻿namespace Blazor.Models
{   
    /// <summary>
    /// Represents an item.
    /// </summary>
    public class Item
    {
        public int Id { get; set; }
        public string DisplayName { get; set; }
        public string Name { get; set; }
        public int StackSize { get; set; }
        public int MaxDurability { get; set; }
        public List<string> EnchantCategories { get; set; }
        public List<string> RepairWith { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string ImageBase64 { get; set; }

        /// <summary>
        /// Constructor for Item.
        /// </summary>
        public Item()
        {
            Id = 2;
            DisplayName = "Dirt";
            Name = "Block_of_dirt";
            StackSize = 64;
            MaxDurability= 9999;
            EnchantCategories = new List<string>();
            RepairWith = new List<string>();
            CreatedDate= DateTime.Now;
            UpdatedDate = DateTime.Now;
        }
    }
}
